#include <iostream>
#include "morse_riddle.hpp"
#include "fluid_auto.hpp"
#include <boost/program_options.hpp>
#include <fstream>

#define HELP "help"
#define VERSION "version"
#define CFG "config"
#define SYNTH_CHAN "synth.channel"
#define SYNTH_KEY "synth.key"
#define SYNTH_VEL "synth.velocity"
#define SYNTH_PROGRAM "synth.program"
#define DEV "synth.device"
#define SF "synth.soundfont"
#define MORSE_PIN "morse-pin"
#define SOLVED_PIN "solved-pin"

#define BROKER "mqtt.broker"
#define CLIENT_ID "mqtt.client-id"

#define SOUND "sound"

int main(int argc, char *argv[])
{
    namespace po = boost::program_options;
    using std::cout;
    using std::cerr;
    using std::endl;
    using std::ifstream;
    typedef std::string str;
    typedef unsigned uns;

    po::options_description generic("Generic options");
    generic.add_options()
        (HELP ",h", "produce help message")
        (VERSION ",v", "print version string")
        (CFG ",c", po::value<str>()->value_name("FILE"), "configuration file")
    ;

    po::options_description config("Configuration");
    config.add_options()
        (BROKER ",B", po::value<str>()->value_name("URI")->default_value("tcp://localhost:1883/"), "mqtt broker uri")
        (CLIENT_ID ",C", po::value<str>()->value_name("NAME")->default_value("morse"), "this client's id")
        /* (LP ",l", po::value<str>()->value_name("NAME")->default_value("launchpad"), "launchpad node")
        (KAR ",k", po::value<str>()->value_name("NAME")->default_value("karaoke"), "karaoke node")
        (BEER ",s", po::value<str>()->value_name("NAME")->default_value("beer"), "beer node")
        (ARD_A_DEV ",A", po::value<str>()->value_name("DEV")->default_value("/dev/ttyUSB0"), "arduino A device")
        (ARD_A_BAUD ",a", po::value<uns>()->value_name("BAUD")->default_value(115200), "arduino A baud rate")
        (ARD_B_DEV ",B", po::value<str>()->value_name("DEV"), "arduino B device")
        (ARD_B_BAUD ",b", po::value<uns>()->value_name("BAUD")->default_value(115200), "arduino B port")
        (QLAB_HOST ",Q", po::value<str>()->value_name("HOST")->default_value("qlab"), "qlab hostname")
        (QLAB_PORT ",q", po::value<uns>()->value_name("PORT")->default_value(53000), "qlab port")
        (OLA_UNI ",o", po::value<uns>()->value_name("NUM")->default_value(1), "ola universe")
        (OLA_SCENES ",d", po::value<str>()->value_name("FILE"), "csv file with dmx scenes") */
        (SYNTH_CHAN ",H", po::value<uns>()->value_name("CHAN")->default_value(0), "synth channel")
        (SYNTH_KEY ",K", po::value<uns>()->value_name("KEY")->default_value(48), "synth key")
        (SYNTH_VEL ",V", po::value<uns>()->value_name("VEL")->default_value(100), "synth velocity")
        (SYNTH_PROGRAM ",P", po::value<uns>()->value_name("PROGRAM")->default_value(0), "synth program (instrument)")
        (SF ",S", po::value<str>()->value_name("FILE")->default_value("/usr/share/sounds/sf2/default-GM.sf2"), "soundfont for the synthesizer")
        (DEV ",D", po::value<str>()->value_name("DEVICE")->default_value("default"), "audio device for the synthesizer output")
        (MORSE_PIN ",m", po::value<uns>()->value_name("PIN")->default_value(6), "morse input pin")
        (SOLVED_PIN ",s", po::value<uns>()->value_name("PIN")->default_value(7), "riddle solved output pin")
        (SOUND ",o", po::value<str>()->value_name("FILE"), "riddle solved sound")
    ;

    po::options_description cmdline_opts;
    cmdline_opts.add(generic).add(config);

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, cmdline_opts), vm);

    str progname = ((argc >= 1) ? argv[0] : "NONAME");

    if (vm.count(HELP)) {
        cout << "Usage: " << progname << " [OPTION]...\n" << cmdline_opts << endl;
        return 0;
    }

    if (vm.count(VERSION)) {
        cout << progname << " version " << "0.0"  << endl;
        return 0;
    }

    if (vm.count(CFG)) {
        const str &cfg_file = vm[CFG].as<str>();
        ifstream cfg_stream(cfg_file);
        if (cfg_stream) {
            po::store(boost::program_options::parse_config_file(cfg_stream, config), vm);
        }
        else {
            cerr << "Error: Cannot open config file " << cfg_file << endl;
            return 1;
        }
    }

    po::notify(vm);

    fluid_settings_auto fluid_settings;
#ifdef __linux__
    fluid_settings.setstr("audio.driver", "alsa");
    fluid_settings.setstr("audio.alsa.device", vm[DEV].as<str>());
    fluid_settings.setint("audio.period-size", 512);
    fluid_settings.setint("audio.periods", 8);
#elif _WIN32
    err = fluid_settings_setstr(ptr, "audio.driver", "dsound");
#else
#error "Please define an audio driver for this system!"
#endif
    int synth_chan = vm[SYNTH_CHAN].as<uns>();
    fluid_synth_auto fluid_synth(fluid_settings);
    fluid_synth.sfload(vm[SF].as<str>(), true);
    fluid_synth.program_change(vm[SYNTH_CHAN].as<uns>(), vm[SYNTH_PROGRAM].as<uns>());
    // fluid_synth.program_select(synth_chan, sfont_id, 2, 80);
    fluid_audio_driver_auto fluid_driver(fluid_settings, fluid_synth);
    str sound;
    if (vm.count(SOUND))
        sound = vm[SOUND].as<str>();
    morse_riddle riddle(
                vm[MORSE_PIN].as<uns>(),
                vm[SOLVED_PIN].as<uns>(),
                &fluid_synth,
                synth_chan,
                vm[SYNTH_KEY].as<uns>(),
                vm[SYNTH_VEL].as<uns>(),
                sound,
                vm[BROKER].as<str>(),
                vm[CLIENT_ID].as<str>());
    riddle();
    return 0;
}
