#ifndef MORSE_RIDDLE_HPP
#define MORSE_RIDDLE_HPP

#include <functional>
#include <list>
#include "pigpiopp.h"
#include <mutex>
#include <condition_variable>
#include "fluid_auto.hpp"
#include "mqtt_node.hpp"
#include <SFML/Audio.hpp>

extern template class mqtt_extras::mqtt_node< boost::asio::io_context::executor_type >;
extern template class mqtt_extras::mqtt_node_extension< boost::asio::io_context::executor_type >;

class morse_riddle : private mqtt_extras::mqtt_node_extension< boost::asio::io_context::executor_type > {
    enum {
        SHORT,
        LONG
    };

    static const bool correct_sequence[];

    std::mutex mymutex;
    std::condition_variable cv;
    sf::SoundBuffer sound_buffer;
    sf::Sound sound;

    void on_run() override {
    }

    static constexpr char TOPIC_SIGNAL [] = "signal";
    static constexpr char TOPIC_RIDDLESOLVED [] = "riddlesolved";

    void on_connected(const std::string& cause) override
    {
        using std::lock_guard;
        using std::mutex;
        using std::cout;
        using std::endl;

        (void) cause;
        lock_guard<mutex> l(mymutex);

        try {
            cout << "Subscribing to signal topic..." << endl;
            mqtt_node_ptr->subscribe(mqtt_node_ptr->full_topic(TOPIC_SIGNAL), 0);
            cout << "Done." << endl;
            cout << "Publishing riddle not solved..." << endl;
            mqtt_node_ptr->publish(mqtt_node_ptr->full_topic(TOPIC_RIDDLESOLVED), "0", 0, true);
            cout << "Done." << endl;
        }
        catch (const mqtt::exception &e) {
            std::cout << "Exception: " << e.what() << std::endl;
        }
    }

    void on_connection_lost(const std::string& cause) override
    {
        (void) cause;
    }

    void on_shutdown() override {}

    void on_message_arrived(mqtt::const_message_ptr msg) override
    {
        using std::string;
        using std::cout;
        using std::endl;
        using std::lock_guard;
        using std::mutex;

        lock_guard<mutex> l(mymutex);

        string topic = msg->get_topic();
        string payload = msg->to_string();
        if (topic == mqtt_node_ptr->full_topic(TOPIC_SIGNAL)) {
            if (payload == "SOLVE") {
                cout << "adminsolved" << endl;
                pigpio.write(solved_pin, 1);
                solved = true;
                history.clear();
                try {
                    cout << "Publishing riddle solved..." << endl;
                    mqtt_node_ptr->publish(mqtt_node_ptr->full_topic(TOPIC_RIDDLESOLVED), "1", 0, true);
                    cout << "Done." << endl;
                }
                catch (const mqtt::exception &e) {
                    std::cout << "Exception: " << e.what() << std::endl;
                }
            }
            else if (payload == "RESET") {
                cout << "riddle reset" << endl;
                pigpio.write(solved_pin, 0);
                solved = false;
                history.clear();
                try {
                    cout << "Publishing riddle not solved..." << endl;
                    mqtt_node_ptr->publish(mqtt_node_ptr->full_topic(TOPIC_RIDDLESOLVED), "0", 0, true);
                    cout << "Done." << endl;
                }
                catch (const mqtt::exception &e) {
                    std::cout << "Exception: " << e.what() << std::endl;
                }
            }
        }
    }

    int morse_pin;
    int solved_pin;
    bool solved;
    std::list<std::uint32_t> history; // durations of presses
    std::uint32_t press_time;
    int synth_chan;
    int synth_key;
    int synth_velocity;
    fluid_synth_auto *synth;
    pigpiopp::client pigpio;
    boost::asio::io_context ioc;
    mqtt_extras::mqtt_node< boost::asio::io_context::executor_type > mqtt_client;

    void pin_callback(unsigned gpio, unsigned level, std::uint32_t tick);

public:
    morse_riddle(int morse_pin, int solved_pin, fluid_synth_auto *synth, int synth_chan, int synth_key, int synth_velocity, const std::string &sound, const std::string &broker_uri, const std::string &client_id)
        : morse_pin(morse_pin)
        , solved_pin(solved_pin)
        , solved(false)
        , synth_chan(synth_chan)
        , synth_key(synth_key)
        , synth_velocity(synth_velocity)
        , synth(synth)
        , pigpio()
        , ioc()
        , mqtt_client(ioc.get_executor(), broker_uri, client_id, this)
    {
        if (!sound_buffer.loadFromFile(sound)) {
            throw std::runtime_error("Cannot load sound " + sound);
        }
        this->sound.setBuffer(sound_buffer);
    }

    /* morse_riddle()
    {} */

    void operator() ();
};


#endif // MORSE_RIDDLE_HPP
