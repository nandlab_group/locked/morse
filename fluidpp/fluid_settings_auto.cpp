#include "fluid_auto.hpp"

fluid_settings_auto::fluid_settings_auto()
{
    ptr = new_fluid_settings();
    if (!ptr) {
        throw fluid_exception();
    }
}

fluid_settings_auto::fluid_settings_auto(fluid_settings_auto &&rhs) noexcept {
    delete_fluid_settings(ptr);
    ptr = rhs.ptr;
    rhs.ptr = nullptr;
}

void fluid_settings_auto::operator =(fluid_settings_auto &&rhs) noexcept {
    delete_fluid_settings(ptr);
    ptr = rhs.ptr;
    rhs.ptr = nullptr;
}

fluid_settings_auto::~fluid_settings_auto()
{
    delete_fluid_settings(ptr);
}

void fluid_settings_auto::setstr(const std::string &name, const std::string &str) const {
    setstr(name.data(), str.data());
}

void fluid_settings_auto::setstr(const char *name, const char *str) const {
    if (fluid_settings_setstr(ptr, name, str)) {
        throw fluid_exception();
    }
}

void fluid_settings_auto::setint(const std::string &name, int val) const {
    setint(name.data(), val);
}

void fluid_settings_auto::setint(const char *name, const int val) const {
    if (fluid_settings_setint(ptr, name, val)) {
        throw fluid_exception();
    }
}
