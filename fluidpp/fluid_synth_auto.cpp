#include "fluid_auto.hpp"

typedef fluid_synth_auto self;

self::fluid_synth_auto(fluid_settings_auto &fluid_settings)
{
    ptr = new_fluid_synth(fluid_settings.ptr);
    if (!ptr) {
        throw fluid_exception();
    }
}

self::fluid_synth_auto(fluid_synth_auto &&rhs) noexcept {
    delete_fluid_synth(ptr);
    ptr = rhs.ptr;
    rhs.ptr = nullptr;
}

void self::operator =(fluid_synth_auto &&rhs) noexcept {
    delete_fluid_synth(ptr);
    ptr = rhs.ptr;
    rhs.ptr = nullptr;
}

self::~fluid_synth_auto()
{
    delete_fluid_synth(ptr);
}

void self::noteon(int chan, int key, int vel) const {
    if (fluid_synth_noteon(ptr, chan, key, vel)) {
        throw fluid_exception();
    }
}

int self::noteoff(int chan, int key) const noexcept {
    return fluid_synth_noteoff(ptr, chan, key);
}

void self::all_notes_off(int chan) const {
    if (fluid_synth_all_notes_off(ptr, chan)) {
        throw fluid_exception();
    }
}

void self::all_sounds_off(int chan) const {
    if (fluid_synth_all_sounds_off(ptr, chan)) {
        throw fluid_exception();
    }
}

void self::program_change(int chan, int program) const {
    if (fluid_synth_program_change(ptr, chan, program)) {
        throw fluid_exception();
    }
}

void self::program_select(int chan, int sfont_id, int bank_num, int preset_num) const {
    if (fluid_synth_program_select(ptr, chan, sfont_id, bank_num, preset_num)) {
        throw fluid_exception();
    }
}

void self::pitch_bend(int chan, int val) const {
    if (fluid_synth_pitch_bend(ptr, chan, val)) {
        throw fluid_exception();
    }
}

int self::sfload(const std::string &filename, int reset_presets) const {
    return sfload(filename.data(), reset_presets);
}

int self::sfload(const char *filename, int reset_presets) const {
    int sfont_id = fluid_synth_sfload(ptr, filename, reset_presets);
    if (sfont_id == FLUID_FAILED) {
        throw fluid_exception();
    }
    return sfont_id;
}
