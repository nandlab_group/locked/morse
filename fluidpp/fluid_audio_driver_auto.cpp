#include "fluid_auto.hpp"

typedef fluid_audio_driver_auto self;

self::fluid_audio_driver_auto(fluid_settings_auto &settings, fluid_synth_auto &synth)
{
    ptr = new_fluid_audio_driver(settings.ptr, synth.ptr);
    if (!ptr) {
        throw fluid_exception();
    }
}

self::fluid_audio_driver_auto(fluid_audio_driver_auto &&rhs) noexcept {
    delete_fluid_audio_driver(ptr);
    ptr = rhs.ptr;
    rhs.ptr = nullptr;
}

void self::operator =(fluid_audio_driver_auto &&rhs) noexcept {
    delete_fluid_audio_driver(ptr);
    ptr = rhs.ptr;
    rhs.ptr = nullptr;
}

self::~fluid_audio_driver_auto()
{
    delete_fluid_audio_driver(ptr);
}
