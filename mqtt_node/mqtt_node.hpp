#ifndef MQTTNODE_HPP
#define MQTTNODE_HPP

#include <string>
#include <boost/asio/execution_context.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/system/error_code.hpp>
#include <boost/asio/executor_work_guard.hpp>
#include "mqtt/async_client.h"
#include <memory>
#include <mutex>
#include <type_traits>

namespace mqtt_extras {

static const char topic_status   [] = "status";
static const char status_online  [] = "ONLINE";
static const char status_offline [] = "OFFLINE";
static const char status_lost    [] = "LOST";
static const char status_failed  [] = "FAILED";

constexpr const std::chrono::seconds reconnect_delay (10);

template <typename Executor>
class mqtt_node;

template <typename Executor>
class mqtt_node_extension;

template <typename Executor>
class mqtt_node :
        private mqtt::callback,
        private mqtt::iaction_listener,
        private mqtt::async_client
{
public:
    typedef Executor executor_type;

private:
    mutable std::mutex m;

    executor_type executor;

    bool shutting_down;

    boost::asio::steady_timer reconnect_timer;
    boost::asio::signal_set system_signals;

    mqtt::connect_options connect_options;

    mqtt_node_extension<executor_type> *extension;

    std::string debug_prefix();

    void system_signal_handler(const boost::system::error_code &ec, int signal);

    // Main client callbacks
    void connected(const std::string& cause) override;

    void connection_lost(const std::string& cause) override;

    void message_arrived(mqtt::const_message_ptr msg) override;

    // Initial connect callbacks
    void on_failure(const mqtt::token &token) override;

    void on_success(const mqtt::token &token) override;

    void start_reconnect_timer();

    std::unique_ptr< boost::asio::executor_work_guard< executor_type > > work_guard;

public:
    /**
     * @brief Prepend "<client_id>/" to the topic
     * @param topic
     * @return full topic path
     */
    std::string full_topic(const std::string &topic) {
        return get_client_id() + '/' + topic;
    }

    /* mqtt_node()
        : async_client("", "")
    {} */

    mqtt_node(
            const executor_type &executor,
            const std::string &broker_uri,
            const std::string &client_id,
            mqtt_node_extension<executor_type> *extension = nullptr)
        : executor(executor)
        , async_client(broker_uri, client_id)
        , m()
        , shutting_down(false)
        , reconnect_timer(executor)
        , system_signals(executor, SIGINT, SIGTERM)
        , connect_options(
              mqtt::connect_options_builder()
              .automatic_reconnect(false)
              .will(mqtt::message(full_topic(topic_status), status_lost, 0, true))
              .finalize())
        , extension(extension)
        , work_guard() // dummy work guard
    {
        set_callback(*this);
        if (extension)
            extension->mqtt_node_ptr = this;
    }

    template <typename ExecutionContext>
    mqtt_node(
            ExecutionContext &execution_context,
            const std::string &broker_uri,
            const std::string &client_id,
            mqtt_node_extension<executor_type> *extension = nullptr,
            typename std::enable_if<
              std::is_convertible<ExecutionContext&, boost::asio::execution_context&>::value
            >::type* = 0)
        : executor(execution_context.get_executor())
        , async_client(broker_uri, client_id)
        , m()
        , shutting_down(false)
        , reconnect_timer(execution_context)
        , system_signals(execution_context, SIGINT, SIGTERM)
        , connect_options(
              mqtt::connect_options_builder()
              .automatic_reconnect(false)
              .will(mqtt::message(full_topic(topic_status), status_lost, 0, true))
              .finalize())
        , extension(extension)
        , work_guard() // dummy work guard
    {
        set_callback(*this);
        if (extension)
            extension->mqtt_node_ptr = this;
    }

    using async_client::get_client_id;
    using async_client::get_server_uri;
    using async_client::is_connected;
    using async_client::publish;
    using async_client::subscribe;

    ~mqtt_node() override;
    void run();
    void shutdown();
};

template <typename Executor>
class mqtt_node_extension
{
public:
    mqtt_node_extension() {}

    mqtt_node<Executor> *mqtt_node_ptr = nullptr;

    virtual void on_run() {}

    virtual void on_connected(const std::string& cause)
    {
        (void) cause;
    }

    virtual void on_connection_lost(const std::string& cause)
    {
        (void) cause;
    }

    virtual void on_shutdown() {}

    virtual void on_message_arrived(mqtt::const_message_ptr msg)
    {
        (void) msg;
    }
};
}

#endif // MQTTNODE_HPP
