#include "mqtt_node.hpp"
#include <boost/bind/bind.hpp>
#include <boost/asio/post.hpp>
#include <boost/asio/placeholders.hpp>
#include <chrono>
#include <sstream>
#include <iomanip>

#include "mqtt/client.h"

namespace mqtt_extras {

#define _CL template <typename Executor>
#define _CR mqtt_node<Executor>

_CL _CR ::~mqtt_node() {
    shutting_down = true;
    try {
        disable_callbacks();
        if (is_connected()) {
            publish(full_topic(topic_status), status_failed, 0, true);
            disconnect()->wait();
        }
    }
    catch (const mqtt::exception &e) {
        std::cout << debug_prefix() << "Exception in mqtt_node dtor: " << e.what() << "." << std::endl;
    }
}

namespace _impl {
    static std::string time_str() {
        using namespace std;
        chrono::system_clock::time_point currentTime = chrono::system_clock::now();
        const time_t time = chrono::system_clock::to_time_t(currentTime);
        chrono::milliseconds ms =
                chrono::duration_cast<std::chrono::milliseconds>(currentTime.time_since_epoch())
                % chrono::milliseconds(1000);
        if (time == (time_t)(-1)) {
            return "NOTIME0";
        }
        else {
            const tm *tm = gmtime(&time);
            if (!tm) {
                return "NOTIME1";
            }
            else {
                ostringstream oss;
                oss << put_time(tm, "%Y-%m-%d %H:%M:%S") << "." << setfill('0') << setw(3) << ms.count();
                return oss.str();
            }
        }
    }
}

_CL std::string _CR::debug_prefix() {
    return "[" + _impl::time_str() + "] " + get_client_id() + ": ";;
}

_CL void _CR ::connected(const std::string& cause) {
    using std::mutex;
    using std::lock_guard;
    using std::string;

    lock_guard<mutex> l(m);
    if (!shutting_down) {
        std::cout << debug_prefix() << "Connected";
        if(cause.size()) {
            std::cout << ", cause: " << cause;
        }
        std::cout << std::endl;
        if (extension)
            extension->on_connected(cause);
        try {
            string tstatus = full_topic(topic_status);
            std::cout << debug_prefix() << "Publishing status '" << status_online << "' on topic '" << tstatus << "'" << std::endl;
            publish(tstatus, status_online, 0, true);
        }
        catch (const mqtt::exception &e) {
            std::cout << debug_prefix() << "connected_action() threw " << e.what() << std::endl;
        }
    }
}

_CL void _CR ::connection_lost(const std::string& cause) {
    using std::mutex;
    using std::lock_guard;

    lock_guard<mutex> l(m);
    if (!shutting_down) {
        std::cout << debug_prefix() << "Connection lost!";
        if(cause.size()) {
            std::cout << " Cause: " << cause;
        }
        std::cout << std::endl;
        if (extension)
            extension->on_connection_lost(cause);
        start_reconnect_timer();
    }
}

_CL void _CR::message_arrived(mqtt::const_message_ptr msg) {
    using std::mutex;
    using std::lock_guard;

    lock_guard<mutex> l(m);
    if (!shutting_down) {
        std::cout << debug_prefix() << "Message arrived: " << msg->get_topic() << " " << msg->to_string() << std::endl;
        if (extension)
            extension->on_message_arrived(msg);
    }
}

_CL void _CR::on_failure(const mqtt::token &token) {
    using std::mutex;
    using std::lock_guard;

    (void)token;
    lock_guard<mutex> l(m);
    if (!shutting_down) {
        std::cout << debug_prefix() << "Connection attempt failed!" << std::endl;
        start_reconnect_timer();
    }
}

_CL void _CR::on_success(const mqtt::token &token) {
    using std::mutex;
    using std::lock_guard;

    (void)token;
    lock_guard<mutex> l(m);
    if (!shutting_down) {
        std::cout << debug_prefix() << "Connection success" << std::endl;
    }
}

_CL void _CR::start_reconnect_timer() {
    using std::mutex;
    using std::lock_guard;

    std::cout << debug_prefix() << "Reconnecting in " << reconnect_delay.count() << "s ..." << std::endl;
    reconnect_timer.expires_after(reconnect_delay);
    reconnect_timer.async_wait([this](const boost::system::error_code &ec) {
        if (!ec) {
            lock_guard<mutex> lock(m);
            if (!ec && !shutting_down && !is_connected()) {
                std::cout << debug_prefix() << "Reconnecting now ..." << std::endl;
                // connect(connect_options, nullptr, *this);
                reconnect();
            }
        }
    });
}

_CL void _CR::shutdown() {
    using std::string;

    shutting_down = true;
    system_signals.cancel();
    reconnect_timer.cancel();
    work_guard.reset();
    string tstatus = full_topic(topic_status);
    try {
        std::cout << debug_prefix() << "Publishing status '" << status_offline
                  << "' on topic '" << tstatus << "' ..." << std::endl;
        publish(tstatus, status_offline, 0, true)->wait();
        std::cout << debug_prefix() << "Offline status published" << std::endl;
    }
    catch (const mqtt::exception &e) {
        std::cout << debug_prefix() << "Publishing offline status threw " << e.what() << std::endl;
    }
    try {
        std::cout << debug_prefix() << "Disconnecting from broker ..." << std::endl;
        disconnect()->wait();
        std::cout << debug_prefix() << "Disconnected" << std::endl;
    }
    catch (const mqtt::exception &e) {
        std::cout << debug_prefix() << "Disconnect threw " << e.what() << std::endl;
    }
    try {
        disable_callbacks();
    }
    catch (const mqtt::exception &e) {
        std::cout << debug_prefix() << "disable_callbacks() threw " << e.what() << std::endl;
    }
    if (extension)
        extension->on_shutdown();
}

_CL void _CR::run() {
    using std::mutex;
    using std::lock_guard;

    typedef boost::asio::executor_work_guard<executor_type> work_guard_t;
    {
        lock_guard<mutex> l(m);
        work_guard.reset(new work_guard_t(executor));
        std::cout << debug_prefix() << "Listening for signals ..." << std::endl;
        system_signals.async_wait([this](const boost::system::error_code &ec, int signal){
            lock_guard<mutex> l(m);
            if (!ec && !shutting_down) {
                std::cout << debug_prefix() << "Signal " << signal << " received" << std::endl;
                shutdown();
            }
        });
        std::cout << "Connecting to the mqtt broker ..." << std::endl;
        connect(connect_options, nullptr, *this);
        std::cout << "Done." << std::endl;
        if (extension)
            extension->on_run();
    }
}

}
