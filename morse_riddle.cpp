#include "morse_riddle.hpp"

#include <iterator>
#include <cassert>

const bool morse_riddle::correct_sequence[] = {LONG, SHORT, SHORT, SHORT, LONG, SHORT};

#define SEQLEN (sizeof(correct_sequence) / sizeof(*correct_sequence))

#include <iostream>

using std::ostream;

// It is a LegacyInputIterator type
template <typename It>
void print_morse(ostream &out, It begin, It end) {
    for (It iter = begin; iter < end; iter++) {
        if (iter != begin) {
            out << " ";
        }
        out << (*iter ? "-" : ".");
    }
}

void morse_riddle::pin_callback(unsigned gpio, unsigned level, std::uint32_t tick) {
    using std::size_t;
    using std::uint32_t;
    using std::cbegin;
    using std::cend;
    using std::cout;
    using std::endl;
    using std::lock_guard;
    using std::mutex;

    lock_guard<mutex> l(mymutex);

    (void) gpio;

    if (!solved) {
        cout << (level ? "HIGH" : "LOW") << endl;

        static const int active = 0;
        if (level == active) {
            synth->noteon(synth_chan, synth_key, synth_velocity);
            press_time = tick;
        }
        else {
            synth->noteoff(synth_chan, synth_key);
            history.emplace_back(tick - press_time);
            size_t size = history.size();
            if (history.size() >= SEQLEN) {
                // Remove old elements from history
                while (size > SEQLEN) {
                    history.pop_front();
                    size--;
                }

                // Get longest press
                uint32_t longest_press = 0;
                for (uint32_t press : history) {
                    if (press > longest_press)
                        longest_press = press;
                }

                uint32_t thresh_long = longest_press / 2;

                // Normalize history
                bool sequence[SEQLEN];
                {
                    auto history_iter = cbegin(history);
                    bool *sequence_iter = sequence;
                    const bool * const sequence_end = sequence_iter + SEQLEN;
                    // assert(cend(history) - cbegin(history) == SEQLEN);
                    do {
                        *sequence_iter = (*history_iter >= thresh_long);
                        ++history_iter;
                        ++sequence_iter;
                    }
                    while (sequence_iter < sequence_end);
                }

                // Print normalized history for debugging
                cout << "Sequence: ";
                print_morse(cout, cbegin(sequence), cend(sequence));
                cout << endl;

                // Check wether sequence of short/long presses is correct
                bool correct = true;
                {
                    bool *sequence_iter = sequence;
                    auto correct_sequence_iter = cbegin(correct_sequence);
                    auto correct_sequence_end = cend(correct_sequence);
                    do {
                        if (*correct_sequence_iter != *sequence_iter) {
                            correct = false;
                            break;
                        }
                        ++correct_sequence_iter;
                        ++sequence_iter;
                    }
                    while (correct_sequence_iter < correct_sequence_end);
                }

                if (correct) {
                    cout << "Morse code entered correctly!" << endl;
                    pigpio.write(solved_pin, 1);
                    sound.play();
                    solved = true;
                    cout << "Publishing riddle solved..." << endl;
                    try {
                        mqtt_node_ptr->publish(mqtt_node_ptr->full_topic(TOPIC_RIDDLESOLVED), "1", 0, true);
                    }
                    catch (const mqtt::exception &e) {
                        std::cout << "Exception: " << e.what() << std::endl;
                    }

                    cout << "Done." << endl;
                }
            }
        }
    }
}

void morse_riddle::operator() () {
    using std::bind;
    using namespace std::placeholders;
    using std::unique_lock;
    using std::mutex;
    using std::cout;
    using std::endl;

    using std::cbegin;
    using std::cend;

    using pigpiopp::INPUT;
    using pigpiopp::OUTPUT;
    using pigpiopp::PUD_UP;

    {
        unique_lock<mutex> l(mymutex);

        cout << "The correct morse code is: ";
        print_morse(cout, cbegin(correct_sequence), cend(correct_sequence));
        cout << endl;

        pigpio = pigpiopp::client(nullptr, nullptr);

        pigpio.set_mode(morse_pin, INPUT);
        pigpio.set_pull_up_down(morse_pin, PUD_UP);
        pigpio.set_mode(solved_pin, OUTPUT);

        pigpio.write(solved_pin, 0);

        pigpio.set_glitch_filter(morse_pin, 50000);
        pigpio.register_callback(morse_pin, pigpiopp::EITHER_EDGE, bind(&morse_riddle::pin_callback, this, _1, _2, _3));

        mqtt_client.run();
    }

    ioc.run();

    // Sleep this thread and never wake up
    // cv.wait(l, []{ return false; });
}
